(describe list
          (check* (list 1 2 3) (1 2 3))
          (check* (list (+ 1 1) (+ 2 2) (+ 3 3)) (2 4 6)))

(describe cons
          (check (cons 'a 'b) '(a . b))
          (check (cons 'a '(b c)) '(a b c)))

(describe cons*
          (check (cons* 'a 'b 'c) '(a b . c))
          (check (cons* 'a 'b '(c d)) '(a b c d))
          (check (cons* 'a) 'a))

(describe make-list
          (check (make-list 4 'c) '(c c c c)))

(describe iota
          (check (iota 5) '(0 1 2 3 4))
          (check (iota 5 0 -3) '(0 -3 -6 -9 -12)))

