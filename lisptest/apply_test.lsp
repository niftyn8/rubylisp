(describe apply
          (check (apply + 1 2 3) 6)
          (check* (apply list '(1 2 3)) (1 2 3))
          )
