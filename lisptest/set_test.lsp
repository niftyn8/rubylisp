(define x 4)

(describe set-in-global-context
          (check x 4)
          (check (begin (set! x 10)
                        x)
                 10)
          (check x 10))

(define y 5)

(describe set-in-local-context
          (check y 5)
          (check (let ((y 2))
                   (set! y 15)
                   y)
                 15)
          (check y 5))

(describe set-car
          (check (let ((pair '(a b)))
                   (set-car! pair 1)
                   (car pair))
                 1))

(describe set-cdr
          (check (let ((pair '(a b)))
                   (set-cdr! pair 1)
                   (cdr pair))
                 1))

(describe set-nth
          (check (set-nth! 2 '(1 2 3) 42) '(1 42 3)))


