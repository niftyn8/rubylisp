(describe simple-let
          (check (let ())
                 nil)
          (check (let ()
                   42)
                 42))

(describe let-with-multiple-expr-body
          (check (let ()
                   1
                   2)
                 2))

(describe let-bindings
          (check (let ((x 1)
                       (y 2))
                   (+ x y))
                 3)
          (check (let* ((x 1)
                       (y (+ x 1)))
                   y)
                 2))


(describe let-binding-scope
          (check (begin (let ((zz 2)) zz)
                        zz)
                 nil))

(describe let-let*
          (define x 10)
          (check (let ((x 5)
                        (y (* x 2)))
                   (+ x y))
                 25)
          (check (let* ((x 5)
                        (y (* x 2)))
                   (+ x y))
                 15))
