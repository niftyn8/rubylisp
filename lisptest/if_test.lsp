(describe if-with-false-condition
          (check (if #f 1) nil)
          (check (if #f 1 2) 2))

(describe if-with-true-condition
          (check (if #t 1) 1)
          (check (if #t 1 2) 1))
