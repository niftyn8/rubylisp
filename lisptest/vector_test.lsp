(describe vector-syntax
          (check '[1 2] '(make-vector 1 2)))

(describe vector-rendering
          (check (str (make-vector 1 2)) "[1 2]"))

(describe vector-to-list
          (check (list [1 2 3]) '(1 2 3)))

(describe list-to-vector
          (check (vector '(1 2 3)) [1 2 3]))

(describe vector-access
          (define v [1 2 3 4 5 6 7 8 9 10])
          (check (first v) 1)
          (check (second v) 2)
          (check (third v) 3)
          (check (fourth v) 4)
          (check (fifth v) 5)
          (check (sixth v) 6)
          (check (seventh v) 7)
          (check (eighth v) 8)
          (check (ninth v) 9)
          (check (tenth v) 10)
          (check (nth 1 v) 1)
          (check (nth 2 v) 2)
          (check (nth 3 v) 3)
          (check (nth 4 v) 4)
          (check (nth 5 v) 5)
          (check (nth 6 v) 6)
          (check (nth 7 v) 7)
          (check (nth 8 v) 8)
          (check (nth 9 v) 9)
          (check (nth 10 v) 10)
)

(describe vector-mutation
          (check (set-nth! 2 [1 2] 42) [1 42]))


(describe vector-head-tail
          (check (head [1 2 3 4]) 1)
          (check (tail [1 2 3 4]) [2 3 4]))

(describe vector-take
          (define v [1 2 3 4 5])
          (check (take 0 v) [])
          (check (take 1 v) [1])
          (check (take 2 v) [1 2])
          (check (take 3 v) [1 2 3])
          (check (take 4 v) [1 2 3 4])
          (check (take 5 v) [1 2 3 4 5]))

(describe vector-drop
          (define v [1 2 3 4 5])
          (check (drop 0 v) [1 2 3 4 5])
          (check (drop 1 v) [2 3 4 5])
          (check (drop 2 v) [3 4 5])
          (check (drop 3 v) [4 5])
          (check (drop 4 v) [5])
          (check (drop 5 v) []))

(describe vector-sublist
          (check (sublist [1 2 3 4] 1 2) [1 2])
          (check (sublist [1 2 3 4] 2 3) [2 3]))

(describe vector-filter
          (check (filter odd? [1 2 3 4 5 6]) [1 3 5])
          (check (filter even? [1 2 3 4 5 6]) [2 4 6]))

(describe vector-remove
          (check (remove odd? [1 2 3 4 5 6]) [2 4 6])
          (check (remove even? [1 2 3 4 5 6]) [1 3 5]))

(describe vector-partition
          (check (partition odd? [1 2 3 4 5 6]) (list [1 3 5] [2 4 6])))

(describe map
          (check (map (lambda (x) (+ x 1)) [1 2 3 4]) [2 3 4 5])
          (check (map + [1 2 3 4] [1 1 1 1]) [2 3 4 5]))

(describe reduce
          (check (reduce-left + 0 [1 2 3 4]) 10)
          (check (reduce-left + 0 []) 0)
          (check (reduce-left + 0 [1]) 1)
          (check (reduce-left * 1 [2 3 4]) 24))

(describe any
          (check (any integer? ["a" 3 "b" 2.7]))
          (check! (any integer? ["a" 3.1 "b" 2.7]))
          (check (any < '(3 1 4 1 5)
                        [2 7 1 8 2])))

(describe every
          (check (every integer? [4 3 8 27]))
          (check! (every integer? ["a" 3 "b" 7]))
          (check (every < '(3 1 4 1 0)
                          [5 7 9 8 2])))

(describe append
          (define a [1 2 3])
          (define b [4 5 6])
          (check (append [1 2 3] [4 5 6]) [1 2 3 4 5 6])
          (check (append a b) [1 2 3 4 5 6])
          (check a [1 2 3])
          (check b [4 5 6])
          (check (append [1 2] [3 4] [5 6]) [1 2 3 4 5 6])
          (check (append [1 2] '(3 4) [5 6]) '(1 2 3 4 5 6)))

