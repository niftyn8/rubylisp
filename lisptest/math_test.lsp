(define xx 2)
(define y 8)
(define z 7)

(describe arithmetic-test
          (check (+ 5 5) 10)
          (check (- 10 7) 3)
          (check (* 2 4) 8)
          (check (/ 25 5) 5)
          (check (% 7 5) 2)
          (check (modulo 7 5) 2)
          (check (truncate 5) 5)
          (check (truncate 5.43) 5))

(describe rounding
          (check (round PI) 3.0)
          (check (round 3.5) 4.0)
          (check (round 2.5) 2.0)
          (check (round -2.5) -2.0)
          (check (round -3.4) -3.0)
          (check (round -3.5) -4.0)
          (check (truncate PI) 3.0)
          (check (truncate 3.5) 3.0)
          (check (truncate -3.5) -3.0)
          (check (ceiling PI) 4.0)
          (check (ceiling 3.5) 4.0)
          (check (ceiling -3.5) -3.0)
          (check (floor PI) 3.0)
          (check (floor 3.5) 3.0)
          (check (floor -3.5) -4.0))

(describe number-tests
          (check (even? 0))
          (check (even? 2))
          (check (even? -2))
          (check! (even? 3))
          (check! (even? -3))

          (check! (odd? 0))
          (check! (odd? 2))
          (check! (odd? -2))
          (check (odd? 3))
          (check (odd? -3))

          (check (negative? -1))
          (check! (negative? 0))
          (check! (negative? 1))

          (check! (zero? -1))
          (check (zero? 0))
          (check! (zero? 1))

          (check! (positive? -1))
          (check! (positive? 0))
          (check (positive? 1))

          (check (integer? 1))
          (check! (integer? 1.0))

          (check (float? 1.0))
          (check! (float? 1)))

(describe interval
          (check (interval 1 1) '(1))
          (check (interval 1 2) '(1 2))
          (check (interval 1 5) '(1 2 3 4 5)))

(describe relational-test
          (check (< xx y) #t)
          (check (< y z) #f)
          (check (> xx y) #f)
          (check (> z xx) #t)
          (check (<= xx 2) #t)
          (check (>= z 7) #t)
          (check (not #f) #t)
          (check (not #t) #f)
          (check (or #f #f) #f)
          (check (or #f #f #t) #t)
          (check (and #t #f #t) #f)
          (check (and #t #t #t) #t)
          (check (or (> xx z) (> y z)) #t)
          (check (and (> xx z) (>y z)) #f))

(describe abs
          (check (abs -1) 1)
          (check (abs 0) 0)
          (check (abs -1) 1))

(describe trig
          (check (sin PI) 0.0)
          (check (cos PI) -1.0)
          (check (tan PI) (/ (sin PI) (cos PI)))
          (check (sqrt (ceiling (abs (cos PI)))) 1.0))

(describe min
          (check (min 3 2) 2)
          (check (min 5 -2) -2)
          (check (min -3 -10) -10)
          (check (min 3 7 1 6) 1))

(describe max
          (check (max 3 2) 3)
          (check (max 5 -2) 5)
          (check (max -3 -10) -3)
          (check (max 3 7 1 6) 7))
