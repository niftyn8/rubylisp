describe 'String' do

  it 'defaults to empty string' do
    n = Lisp::String.new
    expect(n.value).to eq ''
  end

  it 'takes the given value' do
    n = Lisp::String.with_value('hello')
    expect(n.value).to eq 'hello'
  end

  it 'lets you set its value' do
    n = Lisp::String.new
    n.set!('hello')
    expect(n.value).to eq 'hello'
  end

  it 'knows what it is' do
    n = Lisp::String.new
    expect(n.string?).to be true
    expect(n.type).to eq :string
  end

  it 'knows what it isnt' do
    n = Lisp::String.new
    expect(n.number?).to be false
    expect(n.symbol?).to be false
    expect(n.pair?).to be false
  end

  it "converts to a string" do
    n = Lisp::String.with_value("test")
    expect(n.to_s).to eq 'test'
  end

end
