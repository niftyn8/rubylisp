describe "Tokenizer" do

  it "tokenizes single character symbols" do
    t = Lisp::Tokenizer.new('a a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'a'
  end

  it "tokenizes multiple character symbols" do
    t = Lisp::Tokenizer.new('abc a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc'
  end

  it "tokenizes symbols with digits" do
    t = Lisp::Tokenizer.new('ab123c a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'ab123c'
  end

  it "tokenizes symbols with dash" do
    t = Lisp::Tokenizer.new('abc-def a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc-def'
  end

  it "tokenizes symbols with question mark" do
    t = Lisp::Tokenizer.new('abc? a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc?'
  end

  it "tokenizes symbols with exclaimation mark" do
    t = Lisp::Tokenizer.new('abc! a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc!'
  end

  it "tokenizes symbols with an equal sign" do
    t = Lisp::Tokenizer.new('abc= a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc='
  end

  it "tokenizes symbols with colon" do
    t = Lisp::Tokenizer.new('ab:c a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'ab:c'
  end

  it "tokenizes symbols with slash" do
    t = Lisp::Tokenizer.new('ab/c a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :FFI_STATIC_SYMBOL
    expect(lit).to eq 'ab/c'
  end

  it "tokenizes symbols with a leading period" do
    t = Lisp::Tokenizer.new('.abc a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :FFI_SEND_SYMBOL
    expect(lit).to eq 'abc'
  end

  it "tokenizes symbols with a trailing period" do
    t = Lisp::Tokenizer.new('abc. a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :FFI_NEW_SYMBOL
    expect(lit).to eq 'abc'
  end

  it "tokenizes symbols with a leading and trailing period" do
    t = Lisp::Tokenizer.new('.abc. a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :FFI_SEND_SYMBOL
    expect(lit).to eq 'abc'
    t.consume_token
    tok, lit = t.next_token
    expect(tok).to eq :PERIOD
    expect(lit).to eq '.'
  end

  it "tokenizes symbols with a embedded period" do
    t = Lisp::Tokenizer.new('abc.a a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc'
    t.consume_token
    tok, lit = t.next_token
    expect(tok).to eq :FFI_SEND_SYMBOL
    expect(lit).to eq 'a'
  end

  it "tokenizes short numbers" do
    t = Lisp::Tokenizer.new('1 a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '1'
  end

  it "tokenizes long numbers" do
    t = Lisp::Tokenizer.new('1234567 a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '1234567'
  end

  it "tokenizes hex numbers" do
    t = Lisp::Tokenizer.new('#x1f a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :HEXNUMBER
    expect(lit).to eq '#x1f'
  end

  it "tokenizes uppercase hex numbers" do
    t = Lisp::Tokenizer.new('#x1F a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :HEXNUMBER
    expect(lit).to eq '#x1F'
  end

  it "tokenizes floating numbers" do
    t = Lisp::Tokenizer.new('1.42 a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :FLOAT
    expect(lit).to eq '1.42'
  end

  it "tokenizes negative numbers" do
    t = Lisp::Tokenizer.new('-1 a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '-1'
  end

  it "tokenizes string" do
    t = Lisp::Tokenizer.new('"hi" a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :STRING
    expect(lit).to eq '"hi"'
  end

  it "tokenizes simple character" do
    t = Lisp::Tokenizer.new('#\a a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :CHARACTER
    expect(lit).to eq 'a'
  end

  it "tokenizes named character" do
    t = Lisp::Tokenizer.new('#\space a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :CHARACTER
    expect(lit).to eq 'space'
  end

  it "tokenizes character code" do
    t = Lisp::Tokenizer.new('#\U+61 a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :CHARACTER
    expect(lit).to eq 'U+61'
  end

  it "tokenizes quote" do
    t = Lisp::Tokenizer.new('\'a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :QUOTE
  end

  it "tokenizes quasiquote" do
    t = Lisp::Tokenizer.new('`a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :BACKQUOTE
  end

  it "tokenizes unquote" do
    t = Lisp::Tokenizer.new(',a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :COMMA
  end

  it "tokenizes unquote-splicing" do
    t = Lisp::Tokenizer.new(',@a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :COMMAAT
  end

  it "tokenizes (" do
    t = Lisp::Tokenizer.new('( a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :LPAREN
  end

  it "tokenizes )" do
    t = Lisp::Tokenizer.new(') a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :RPAREN
  end

  it "tokenizes {" do
    t = Lisp::Tokenizer.new('{ a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :LBRACE
  end

  it "tokenizes }" do
    t = Lisp::Tokenizer.new('} a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :RBRACE
  end

  it "tokenizes [" do
    t = Lisp::Tokenizer.new('[ a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :LBRACKET
  end

  it "tokenizes ]" do
    t = Lisp::Tokenizer.new('] a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :RBRACKET
  end

  it "tokenizes ." do
    t = Lisp::Tokenizer.new('. a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :PERIOD
  end

  it "tokenizes +" do
    t = Lisp::Tokenizer.new('+ a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '+'
  end

  it "tokenizes -" do
    t = Lisp::Tokenizer.new('- a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '-'
  end

  it "tokenizes *" do
    t = Lisp::Tokenizer.new('* a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '*'
  end

  it "tokenizes /" do
    t = Lisp::Tokenizer.new('/ a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '/'
  end

  it "tokenizes %" do
    t = Lisp::Tokenizer.new('% a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '%'
  end

  it "tokenizes <=" do
    t = Lisp::Tokenizer.new('<= a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '<='
  end

  it "tokenizes <" do
    t = Lisp::Tokenizer.new('< a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '<'
  end

  it "tokenizes >=" do
    t = Lisp::Tokenizer.new('>= a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '>='
  end

  it "tokenizes >" do
    t = Lisp::Tokenizer.new('> a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '>'
  end

  it "tokenizes =" do
    t = Lisp::Tokenizer.new('= a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '='
  end

  it "tokenizes !=" do
    t = Lisp::Tokenizer.new('!= a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '!='
  end

  it "tokenizes !" do
    t = Lisp::Tokenizer.new('! a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq '!'
  end

  it "tokenizes #t" do
    t = Lisp::Tokenizer.new('#t a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :TRUE
  end

  it "tokenizes #f" do
    t = Lisp::Tokenizer.new('#f a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :FALSE
  end

  it "skips comment to eof" do
    t = Lisp::Tokenizer.new('; a')
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :EOF
  end

  it "skips comment returning next token" do
    t = Lisp::Tokenizer.new("; a\n42 ")
    t.init
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '42'
  end

end
