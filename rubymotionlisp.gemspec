Gem::Specification.new do |gem|
  gem.name        = 'rubymotionlisp'
  gem.version     = '0.1.4'
  gem.date        = '2014-09-06'
  gem.summary     = "Lisp in Ruby"
  gem.description = "An embeddable Lisp as an extension language for RubyMotion"
  gem.authors     = ["Dave Astels"]
  gem.email       = 'dastels@icloud.com'
  gem.files       = Dir.glob('lib/**/*.rb')
  gem.files      << 'README.md'
  gem.require_paths = ['lib']
  gem.homepage    = 'https://bitbucket.org/dastels/rubylisp' #'http://rubygems.org/gems/rubylisp'
  gem.license     = 'MIT'
end
