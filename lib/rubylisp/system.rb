module Lisp

  class System

    def self.register
      Primitive.register("sleep")         {|args, env| Lisp::System.sleep_impl(args, env) }
    end


    def self.sleep_impl(args, env)
      raise "sleep needs 1 argument" if args.length != 1
      arg = args.car.evaluate(env)
      raise "sleep needs a numeric argument" unless arg.number?
      sleep(arg.value)
    end

    
  end
end
