module Lisp

  class ConsCell < Object
    include Enumerable
    attr_reader :car, :cdr

    def self.cons(a=nil, b=nil)
      ConsCell.new(a, b)
    end

    def initialize(car=nil, cdr=nil)
      @car = car
      @cdr = cdr
    end

    def value
      self
    end
    
    def set_car!(d)
      @car = d
    end

    def lisp_object?
      true
    end
    
    def set_cdr!(d)
      @cdr = d
    end


    def set_nth!(n, d)
      c = self
      (n-1).times {|i| c = c.cdr}
      c.set_car!(d)
    end

    
    def empty?
      @car.nil? && @cdr.nil?
    end

    def string?
      false
    end

    def character?
      false
    end

    def number?
      false
    end

    def positive?
      false
    end

    def zero?
      false
    end

    def negative?
      false
    end

    def symbol?
      false
    end

    def primitive?
      false
    end

    def special?
      false
    end

    def function?
      false
    end

    def macro?
      false
    end

    def pair?
      true
    end

    def list?
      true
    end

    def alist?
      false
    end

    def frame?
      false
    end

    def vector?
      false
    end
    
    def eq?(sexpr)
      return false unless sexpr.pair?
      (@car == sexpr.car || @car.eq?(sexpr.car)) && (@cdr == sexpr.cdr || @cdr.eq?(sexpr.cdr))
    end
    
    def type
      :pair
    end

    def to_s_helper
      return "#{@car.to_s}" if @cdr.nil?
      return "#{@car.to_s} . #{@cdr.to_s}" unless @cdr.pair?
      "#{@car.to_s} #{@cdr.to_s_helper}"
    end

    def to_s
      return "()" if self.empty?
      return "'#{@cdr.car.to_s}" if @car.symbol? && @car.name == "quote"
      return "{#{@cdr.to_s_helper}}" if @car.symbol? && @car.name == "make-frame"
      return "[#{@cdr.to_s_helper}]" if @car.symbol? && @car.name == "make-vector"
      return "(#{@car.to_s} . #{@cdr.to_s})" if !@cdr.nil? && !@cdr.pair?
      return "(#{self.to_s_helper})"
    end

    def print_string_helper
      @cdr.nil? ? "#{@car.print_string}" : "#{@car.print_string} #{@cdr.print_string_helper}"
    end

    def print_string
      return "()" if self.empty?
      return "'#{@cdr.car.print_string}" if @car.symbol? && @car.name == "quote"
      return "{#{@cdr.print_string_helper}}" if @car.symbol? && @car.name == "make-frame"
      return "[#{@cdr.print_string_helper}]" if @car.symbol? && @car.name == "make-vector"
      return "(#{@car.print_string} . #{@cdr.print_string})" if !@cdr.nil? && !@cdr.pair?
      return "(#{self.print_string_helper})"
    end

    def to_a
      a = []
      c = self
      until c.nil?
        a << c.car()
        c = c.cdr
      end
      a
    end

    def each &block
      c = self
      if self.length > 0
        until c.nil?
          yield c.car 
          c = c.cdr
        end
      end
    end

    def self.array_to_list(cells, tail=nil)
      return tail if cells.empty?
      head = ConsCell.new
      last_cell = head
      cells.each do |d|
        new_cell = self.cons(d, nil)
        last_cell.set_cdr!(new_cell)
        last_cell = new_cell
      end
      last_cell.set_cdr!(tail)
      head.cdr
    end

    def traverse(path)
      return self if path.empty?
      next_cell = (path[0] == ?a) ? @car : @cdr
      return next_cell if path.length == 1
      return nil if next_cell.nil?  || !next_cell.pair?
      next_cell.traverse(path[1..-1])
    end

    def method_missing(name, *args, &block)
      if name[0] == ?c && name[-1] == ?r && (name[1..-2].chars.all? {|e| "ad".include?(e)})
        self.traverse(name[1..-2].reverse)
      else
        super
      end
    end

    def nth(n)
      c = self
      (n-1).times {|i| c = c.cdr}
      c.car
    end

    def nth_tail(n)
      c = self
      (n-1).times {|i| c = c.cdr}
      c
    end

    def objc_object_or_nil(obj)
      return nil unless obj.object?
      return obj.value
    end
      

    def evaluate(env)
      return self if empty?
      func = @car.evaluate(env)
      raise "There is no function or macro named #{@car}" if func.nil?
      func.apply_to(@cdr, env)
    end

    def evaluate_each(env)
      result = @car.evaluate(env)
      return result if @cdr.nil?
      @cdr.evaluate_each(env)
    end

    def length
      return 0 if empty?
      return 1 if @cdr.nil?
      return 1 + @cdr.length
    end

    def true?
      true
    end

    def false?
      false
    end

    def quoted
      Lisp::ConsCell.array_to_list([Symbol.named("quote"), self])
    end

    def last
      c = self
      while !c.cdr.nil? && c.cdr.pair? do
        c = c.cdr
      end
      c
    end

    def flatten
      ary = to_a.collect {|s| s.list? ? s.to_a : s}
      ConsCell.array_to_list(ary.flatten)
    end
    
    
  end

end
