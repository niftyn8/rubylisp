module Lisp

  class Symbol < Atom

    def self.named(n)
      name = n.to_s
      @@symbols ||= {}
      return @@symbols[n] if @@symbols.has_key?(n)
      s = self.new(n)
      @@symbols[n] = s
      s
    end

    def initialize(n)
      @value = n
      @naked = (n[-1] == ?:)
    end

    def to_naked
      Lisp::Symbol.named("#{@value}:")
    end

    
    def name
      @value
    end

    def symbol?
      true
    end

    def naked?
      @naked
    end
    

    def type
      :symbol
    end

    def evaluate(env)
      return self if @naked
      env.value_of(self)
    end

    def to_s
      @value
    end

    def to_sym
      @value.to_sym
    end

  end

end
