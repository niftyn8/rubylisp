module Lisp

  class FfiNew < Atom

    def initialize(name)
      @value = name
      @klass = Object.const_get(name)
    end

    def apply_to(args, env)
      NativeObject.with_value(@klass.new)
    end

    def apply_to_without_evaluating(args, env)
      NativeObject.with_value(@klass.new)
    end

    def to_s
      "#{@value}."
    end

    def primitive?
      true
    end

    def type
      :primitive
    end

  end

end
