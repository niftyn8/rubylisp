module Lisp

  class EnvironmentFrame

    attr_accessor :frame
    
    def self.global
      @@global_frame ||= EnvironmentFrame.new(nil)
      @@global_frame
    end

    def self.extending(parent, f=nil)
      f ||= parent.frame if parent && parent.has_frame?
      self.new(parent, f)
    end

    def initialize(parent, f=nil)
      @bindings = []
      @parent = parent
      @frame = f
    end

    def has_frame?
      !@frame.nil?
    end
    
    # Bindings following parent env frame pointer

    def is_name_bound?(str)
      if !@frame && @frame.has_slot?(Lisp:Symbol.named("#{str}:", true))
        return true
      end
        
      binding = @bindings.detect {|b| b.symbol.name == str}
      return true unless binding.nil?
      return false if @parent.nil?
      return @parent.is_name_bound?(str)
    end

    def binding_for(symbol)
      binding = @bindings.detect {|b| b.symbol.name == symbol.name}
      return binding unless binding.nil?
      return @parent.binding_for(symbol) unless @parent.nil?
      nil
    end

    def bind(symbol, value)
      b = self.binding_for(symbol)
      if b.nil?
        @bindings << Lisp::Binding.new(symbol, value)
      else
        b.value = value
      end
    end

    def set(symbol, value)
      naked_symbol = symbol.to_naked
      if @frame && @frame.has_slot?(naked_symbol)
        return @frame.at_put(naked_symbol, value)
      end
        
      b = self.binding_for(symbol)
      if b.nil?
        raise "#{symbol} is undefined."
      else
        b.value = value
      end
    end

    # Bindings local to this env frame only

    def local_binding_for(symbol)
      @bindings.detect {|b| b.symbol.name == symbol.name}
    end

    def bind_locally(symbol, value)
      b = self.local_binding_for(symbol)
      if b.nil?
        @bindings << Lisp::Binding.new(symbol, value)
      else
        b.value = value
      end
    end

    # Look up a symbol

    def value_of(symbol)
      b = local_binding_for(symbol)
      return b.value unless b.nil?
                
      naked_symbol = symbol.to_naked
      if @frame && @frame.has_slot?(naked_symbol)
        return @frame.get(naked_symbol)
      end

      b = binding_for(symbol)
      return b.value unless b.nil?
      nil
    end
    

    def quick_value_of(symbol_name)
      b = binding_for(Symbol.new(symbol_name))
      b.nil? ? nil : b.value
    end
    

    def dump
      @bindings.each do |b|
        puts b.to_s
      end
    end

  end

end
