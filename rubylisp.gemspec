Gem::Specification.new do |s|
  s.name        = 'rubylisp'
  s.version     = '0.1.1'
  s.bindir      = 'bin'
  s.executables << 'rubylisp'
  s.date        = '2014-09-06'
  s.summary     = "Lisp in Ruby"
  s.description = "An embeddable Lisp as an extension language for Ruby"
  s.authors     = ["Dave Astels"]
  s.email       = 'dastels@icloud.com'
  s.files       = ["lib/rubylisp.rb",
                   "lib/rubylisp/alist.rb",
                   "lib/rubylisp/assignment.rb",
                   "lib/rubylisp/atom.rb",
                   "lib/rubylisp/binding.rb",
                   "lib/rubylisp/boolean.rb",
                   "lib/rubylisp/builtins.rb",
                   "lib/rubylisp/character.rb",
                   "lib/rubylisp/cons_cell.rb",
                   "lib/rubylisp/environment_frame.rb",
                   "lib/rubylisp/equivalence.rb",
                   "lib/rubylisp/exception.rb",
                   "lib/rubylisp/ext.rb",
                   "lib/rubylisp/ffi_class.rb",
                   "lib/rubylisp/ffi_new.rb",
                   "lib/rubylisp/ffi_send.rb",
                   "lib/rubylisp/ffi_static.rb",
                   "lib/rubylisp/frame.rb",
                   "lib/rubylisp/function.rb",
                   "lib/rubylisp/io.rb",
                   "lib/rubylisp/list_support.rb",
                   "lib/rubylisp/logical.rb",
                   "lib/rubylisp/macro.rb",
                   "lib/rubylisp/math.rb",
                   "lib/rubylisp/number.rb",
                   "lib/rubylisp/object.rb",
                   "lib/rubylisp/parser.rb",
                   "lib/rubylisp/primitive.rb",
                   "lib/rubylisp/relational.rb",
                   "lib/rubylisp/special_forms.rb",
                   "lib/rubylisp/string.rb",
                   "lib/rubylisp/symbol.rb",
                   "lib/rubylisp/system.rb",
                   "lib/rubylisp/testing.rb",
                   "lib/rubylisp/tokenizer.rb",
                   "lib/rubylisp/type_checks.rb",
                   "lib/rubylisp/vector.rb"]
  s.homepage    = 'https://bitbucket.org/dastels/rubylisp'
  s.license     = 'MIT'
end
